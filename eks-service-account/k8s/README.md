# cloudgeeks.ca 

### awscli commands

- Create S3 Bucket
```s3-mb
aws s3api create-bucket --bucket cloudgeeks-prod --region us-east-1
```

- S3 Bucket Versioning Enable
```s3-versioning
aws s3api put-bucket-versioning --bucket cloudgeeks-prod --versioning-configuration Status=Enabled
```

- S3 Copy File
```s3-cp
aws s3 cp README.md s3://cloudgeeks-prod/README.md
```

- S3 List Files
```s3-ls
aws s3 ls s3://cloudgeeks-prod/
```

- Kubectl commands tp get pods
```kubectl-get-pods
kubectl get pods -n cloudgeeks
```
- kubeclt commands to get logs, replace pod name with your pod name
```kubectl-get-logs
kubectl logs pod/nginx-deployment-69667f788d-szjs8 -c aws-cli -n cloudgeeks
```

##### Two Types of Terraform EKS Service Accounts (Security Perspective)

- iam-eks-role-hardening-with-oidc-to-specific-namespace
```terraform-eks-service-account-hardening
module "iam-eks-role-hardening-with-oidc-to-specific-namespace" {
  source    = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  role_name = "cloudgeeks-prod"

  role_policy_arns = {
    "AmazonS3FullAccess" = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
    }
  
    oidc_providers       = {
      one = {
        provider_arn               = module.eks.oidc_provider_arn
        namespace_service_accounts = ["cloudgeeks:my-app-staging", "canary:service-account-name"]
      }
    }

}

```

- iam-assumable-role-with-oidc-just-like-iam-role-attachment-to-ec2
```terraform-eks-service-account-assumable-role
module "iam-assumable-role-with-oidc-just-like-iam-role-attachment-to-ec2" {
    source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
    version = "5.10.0"

    create_role      = true
    role_name        = "cloudgeeks-dev"
    provider_url     = module.eks.cluster_oidc_issuer_url
    role_policy_arns = [
      "arn:aws:iam::aws:policy/AmazonS3FullAccess"
    ]

  }
```