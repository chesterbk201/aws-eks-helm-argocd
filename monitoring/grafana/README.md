# cloudgeeks.ca

- https://github.com/prometheus-operator/kube-prometheus

- Prometheus release-0.11
```release-0.11
git clone --depth 1 https://github.com/prometheus-operator/kube-prometheus.git -b release-0.11
```

- find
```find
mkdir grafana
find /mnt/kube-prometheus/manifests -name "grafana*" -exec cp {} /mnt/grafana/ \;
```

- apply
```apply
kubectl -n monitoring apply -R -f grafana/
```

- Grafana Promethues Dashboard Import 315 3119 9706 3662