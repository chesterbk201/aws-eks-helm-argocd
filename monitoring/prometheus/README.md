# cloudgeeks.ca

### helm bitnami 
- https://github.com/bitnami/charts/tree/main/bitnami/kube-prometheus#configuration-and-installation-details

```bitnami
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```
### kubernetes GKE
- Note: GKE Uses kubeDns, that is why coreDns is set to false

```gke-helm
helm upgrade --install prometheus \
--namespace monitoring --create-namespace \
bitnami/kube-prometheus \
--version 8.3.1 \
--set operator.enabled=true \
--set coreDns.enabled=false \
--values prometheus-values.yaml
```

### kubernetes EKS
- Note: EKS Uses CoreDns, that is why coreDns is set to true

```eks-helm
helm upgrade --install prometheus \
--namespace monitoring --create-namespace \
bitnami/kube-prometheus \
--version 8.3.1 \
--set operator.enabled=true \
--set coreDns.enabled=true \
--values prometheus-values.yaml
```

- Apply Service Monitor
```service-monitor
kubectl apply -f service-monitor.yaml
```

- kubernetes service monitor in namespace monitoring
```k8-service-monitor-namespace-monitoring
kubectl get servicemonitor -n monitoring
```

- kubernetes service monitor in namespace default
```k8-service-monitor-namespace-default
kubectl get servicemonitor -n default
kubectl describe servicemonitor/python-app -n default
```

### Port name
- Note: In service add the name of the port, here I added web